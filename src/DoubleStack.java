import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;

public class DoubleStack {

   public static void main (String[] argum) {
   }

   private LinkedList<Double> mag;

   DoubleStack() {
      mag = new LinkedList<>();
   }

   @Override
   public Object clone() throws CloneNotSupportedException {
      DoubleStack cloned = new DoubleStack();
      if (!stEmpty()) {
         for (int i = 0; i <= mag.size() - 1; i++)
            cloned.mag.add(mag.get(i));
      }
      return cloned;
   }

   public boolean stEmpty() {
      return mag.isEmpty();
   }

   public void push (double a) {
      mag.push(a);
   }

   public double pop() {
      if (stEmpty()) {
         throw new IndexOutOfBoundsException("Stack underflow");
      }
      return mag.pop();
   }

   public void op (String s) {
      if (mag.size() < 2)
         throw new IndexOutOfBoundsException("Too few elements for " + s);
      double n2 = pop();
      double n1 = pop();
      if (s.equals("+"))
         push(n1 + n2);
      else if (s.equals("-"))
         push(n1 - n2);
      else if (s.equals("*"))
         push(n1 * n2);
      else if (s.equals("/"))
         push(n1 / n2);
      else
         throw new IllegalArgumentException("Invalid operation: " + s);
   }
  
   public double tos() {
      if (stEmpty())
         throw new IndexOutOfBoundsException("Stack underflow");
      return mag.peekFirst();
   }

   @Override
   public boolean equals (Object o) {
      if (this.stEmpty() && ((DoubleStack) o).stEmpty()) {
         return true;
      } else if (mag.size() != ((DoubleStack) o).mag.size()) {
         return false;
      }

      for (int i = 0; i < mag.size(); i++) {
         if (!mag.get(i).equals(((DoubleStack) o).mag.get(i))) {
            return false;
         }
      }
      return true;
   }

   @Override
   public String toString() {
      if (stEmpty()) {
         return "empty stack";
      }

      StringBuilder b = new StringBuilder();
      for (int i = mag.size() - 1; i >= 0; i--) {
         b.append(mag.get(i)).append(" ");
      }
      return b.toString();
   }

   public static double interpret (String pol) {
      if (pol == null || pol.equals("") || pol.replaceAll(" ", "").equals("")) {
         throw new RuntimeException("Your expression is empty!");
      }


      pol = pol.replaceAll("\t", "");
      ArrayList<String> exp = new ArrayList<>(Arrays.asList(pol.split(" ")));
      exp.removeAll(Arrays.asList("", null));


      //Koodi idee on võetud https://www.geeksforgeeks.org/evaluate-the-value-of-an-arithmetic-expression-in-reverse-polish-notation-in-java/
      DoubleStack m = new DoubleStack();
      double x, y;
      double result = 0;
      for (String c : exp) {
         if (!c.equals("+") && !c.equals("-") && !c.equals("*") && !c.equals("/") &&
                 !c.equals("SWAP") && !c.equals("ROT") && !c.equals("DUP")) {
            try {
               m.push(Double.parseDouble(c));
            } catch (RuntimeException e){
               throw new RuntimeException(String.format("Illegal symbol %s in expression %s", c, pol));
            }
         } else if (!m.stEmpty() && c.equals("DUP")) {
            m.push(m.tos());
         } else if (m.mag.size() < 2) {
            throw new RuntimeException(String.format("Cannot perform %s in expression %s", c, pol));
         } else if (c.equals("+")) {
            y = m.pop();
            x = m.pop();
            result = x + y;
            m.push(result);
         } else if (c.equals("-")) {
            y = m.pop();
            x = m.pop();
            result = x - y;
            m.push(result);
         } else if (c.equals("*")) {
            y = m.pop();
            x = m.pop();
            result = x * y;
            m.push(result);
         } else if (c.equals("/")) {
            y = m.pop();
            x = m.pop();
            result = x / y;
            m.push(result);
         } else if (c.equals("SWAP")) {
            y = m.pop();
            x = m.pop();
            m.push(y);
            m.push(x);
         } else if (c.equals("ROT")) {
            if (m.mag.size() > 2) {
               y = m.pop();
               x = m.pop();
               double z = m.pop();
               m.push(x);
               m.push(y);
               m.push(z);
            }
         }
      }
      if (m.mag.size() == 1) {
         return m.mag.get(0);
      } else if (m.mag.size() > 1) {
         throw new RuntimeException(String.format("Too many numbers in expression %s", pol));
      }
      return result;
   }

}

